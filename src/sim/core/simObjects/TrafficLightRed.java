package sim.core.simObjects;

import sim.core.Manager;
import sim.core.SimEvent;

public class TrafficLightRed extends SimEvent {
    private final double greenLightInterval = 2.0;

    public TrafficLightRed(Manager simMngr, double time) {
        super(simMngr, time);
    }

    @Override
    public void stateChange() {
        new TrafficLightGreen(getSimMngr(), greenLightInterval);

        System.out.print("runTime: " + getSimMngr().simTime());
        System.out.println(", red light");
    }
}
