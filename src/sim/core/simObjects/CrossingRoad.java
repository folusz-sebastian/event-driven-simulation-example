package sim.core.simObjects;

import sim.core.Manager;
import sim.core.SimEvent;
import sim.random.SimGenerator;

public class CrossingRoad extends SimEvent {
    private final SimGenerator rnd = new SimGenerator();
    private final int maxNumberOfPeople;

    public CrossingRoad(Manager simMngr, double time, int maxNumberOfPeople) {
        super(simMngr, time);
        this.maxNumberOfPeople = maxNumberOfPeople;
    }

    @Override
    public void stateChange() {
        int numberOfPeople = rnd.uniformInt(100);

        System.out.print("runTime: " + getSimMngr().simTime());
        System.out.print(", number of people: " + numberOfPeople);
        if (numberOfPeople < maxNumberOfPeople) {
            System.out.println(", crossing road");
        }
        else
            System.out.println(", not crossing road because of too many people");
    }
}
