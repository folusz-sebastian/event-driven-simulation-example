package sim.core.simObjects;

import sim.core.Manager;
import sim.core.SimEvent;

public class TrafficLightGreen extends SimEvent {
    private final double redLightInterval = 2.0;

    public TrafficLightGreen(Manager simMngr, double time) {
        super(simMngr, time);
    }

    @Override
    public void stateChange() {
        new TrafficLightRed(getSimMngr(), redLightInterval);
        new CrossingRoad(getSimMngr(), 0, 50);

        System.out.print("runTime: " + getSimMngr().simTime());
        System.out.println(", green light");
    }
}
