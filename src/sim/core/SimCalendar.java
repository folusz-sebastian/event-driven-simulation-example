package sim.core;

import java.util.Comparator;
import java.util.Optional;
import java.util.PriorityQueue;

public class SimCalendar {
    private final PriorityQueue<SimEvent> simEventQueue;

    public SimCalendar() {
        Comparator<SimEvent> comparator = (o1, o2) -> {
            if (o1.getRunTime() != o2.getRunTime())
                return Double.compare(o1.getRunTime(), o2.getRunTime());
            else
                return o1.getSimPriority() - o2.getSimPriority();
        };
        this.simEventQueue = new PriorityQueue<>(comparator);
    }

    public void addEvent(SimEvent event) {
        this.simEventQueue.add(event);
    }

    public Optional<SimEvent> getFirstEvent() {
        SimEvent simEvent = this.simEventQueue.poll();
        return Optional.ofNullable(simEvent);
    }
}
