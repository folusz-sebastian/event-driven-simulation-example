package sim.core;
import java.util.Optional;

public class Manager {
    private double startSimTime = 0.0;
    private double stopSimTime = Double.MAX_VALUE;
    private double currentSimTime = startSimTime;
    private static Manager simMngr; // Singleton
    private boolean simulationStarted = false;
    private final SimCalendar simCalendar = new SimCalendar();

    public static Manager getInstance(double startSimTime) {
        if (simMngr == null) {
            simMngr = new Manager(startSimTime);
        }
        return simMngr;
    }

    private Manager(double startSimTime) {
        if (startSimTime>0.0)
            this.startSimTime = startSimTime;
    }

    public void registerSimEvent(SimEvent event) {
        Optional<SimEvent> simEventOptional = Optional.of(event);
        simCalendar.addEvent(simEventOptional.get());
    }

    public Optional<SimEvent> unRegisterSimEvent() {
        return simCalendar.getFirstEvent();
    }

    public final double simTime() {
        return currentSimTime;
    }

    public final void stopSimulation() {
        simulationStarted = false;
    }

    public final void startSimulation() {
        simulationStarted = true;
        nextEvent();
    }

    private void nextEvent() {
        while (currentSimTime < stopSimTime && simulationStarted) {
            Optional<SimEvent> simEvent = unRegisterSimEvent();
            simEvent.ifPresent(event -> {
                currentSimTime = event.getRunTime();
                event.stateChange();
            });
        }
    }

    public void setEndSimTime(double endSimTime) {
        this.stopSimTime = endSimTime;
    }
}
