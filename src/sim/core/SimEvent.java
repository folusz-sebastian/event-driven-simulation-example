package sim.core;

public abstract class SimEvent {
    private double runTime;
    private int simPriority;
    private Manager simMngr;

    public SimEvent(Manager simMngr, double time) {
        if (simMngr != null) {
            this.simMngr = simMngr;
            simMngr.registerSimEvent(this);
            this.runTime = simMngr.simTime() + time;
        }
    }

    public SimEvent(Manager simMngr, double time, int simPriority) {
        if (simMngr != null) {
            this.simMngr = simMngr;
            this.simPriority = simPriority;
            simMngr.registerSimEvent(this);
            this.runTime = simMngr.simTime() + time;
        }
    }

    public abstract void stateChange();

    public double getRunTime() {
        return runTime;
    }

    public Manager getSimMngr() {
        return simMngr;
    }

    public int getSimPriority() {
        return simPriority;
    }

    public void setSimPriority(int simPriority) {
        this.simPriority = simPriority;
    }
}
