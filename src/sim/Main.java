package sim;

import sim.core.Manager;
import sim.core.simObjects.TrafficLightGreen;

public class Main {
    private static final double startTime = 0.0;
    private static final double endSimTime = 12.0;

    public static void main(String[] args) {
        Manager simMngr = Manager.getInstance(startTime);

        new TrafficLightGreen(simMngr, startTime);

        simMngr.setEndSimTime(endSimTime);
        simMngr.startSimulation();
    }
}
